<?php

namespace Drupal\flush_single_image;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\image\Entity\ImageStyle;

/**
 * Flush Single Image Service.
 */
class FlushSingleImage implements FlushSingleImageInterface {

  /**
   * Removes or unlinks image derivative. Essentially flushing the image style.
   */
  const ACTION_UNLINK = 1;

  /**
   * Generates an image derivative applying all image effects and saving the resulting image.
   */
  const ACTION_REGENERATE = 2;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a new FlushSingleImage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config factory service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, ConfigFactory $configFactory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function flush(string $path, int $action = self::ACTION_UNLINK): array {
    if (!in_array($action, [self::ACTION_UNLINK, self::ACTION_REGENERATE])) {
      throw new \InvalidArgumentException("The argument \$action must be either " . get_class($this) . "::ACTION_UNLINK or " . get_class($this) . "::ACTION_REGENERATE");
    }

    $style_paths = $this->getStylePaths($path);
    $flushed_style_paths = [];
    foreach ($style_paths as $style_id => $style_path) {
      if ($action == self::ACTION_UNLINK) {
        if ($this->fileSystem->unlink($style_path)) {
          $flushed_style_paths[$style_id] = $style_path;
        }
      }
      elseif ($action == self::ACTION_REGENERATE) {
        $imageStyle = ImageStyle::load($style_id);
        if ($imageStyle->createDerivative($path, $style_path)) {
          $flushed_style_paths[$style_id] = $style_path;
        }
      }
    }

    return $flushed_style_paths;
  }

  /**
   * {@inheritdoc}
   */
  public function flushStyle(string $path, string $image_style, int $action = self::ACTION_UNLINK): bool {
    if (!in_array($action, [self::ACTION_UNLINK, self::ACTION_REGENERATE])) {
      throw new \InvalidArgumentException("The argument \$action must be either " . get_class($this) . "::ACTION_UNLINK or " . get_class($this) . "::ACTION_REGENERATE");
    }

    $status = FALSE;
    $style_paths = $this->getStylePaths($path);
    foreach ($style_paths as $style_id => $style_path) {
      if ($style_id == $image_style) {
        if ($action == self::ACTION_UNLINK) {
          $status = $this->fileSystem->unlink($style_path);
        }
        elseif ($action == self::ACTION_REGENERATE) {
          $imageStyle = ImageStyle::load($style_id);
          $status = $imageStyle->createDerivative($path, $style_path);
        }
      }
    }

    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function getStylePaths(string $path): array {
    $path = $this->buildUri($path);
    $styles = $this->entityTypeManager->getStorage('image_style')
      ->loadMultiple();

    $style_paths = [];
    foreach ($styles as $style) {
      /** @var \Drupal\image\Entity\ImageStyle  $style */
      $style_path = $style->buildUri($path);
      if (is_file($style_path) && file_exists($style_path)) {
        $style_paths[$style->id()] = $style_path;
      }
      $path_parts = pathinfo($style_path);
      $style_path_webp = $path_parts['dirname'] . '/' . $path_parts['filename'] . '.webp';
      if (is_file($style_path_webp) && file_exists($style_path_webp)) {
        $style_paths[$style->id()] = $style_path_webp;
      }
    }

    return $style_paths;
  }

  /**
   * Build a URI for a given path if it's missing the scheme.
   *
   * @param string $path
   *   Path given to build uri.
   *
   * @return string
   *   Returns uri.
   */
  protected function buildUri(string $path): string {
    if (!StreamWrapperManager::getScheme($path)) {
      $default_scheme = $this->configFactory->get('system.file')->get('default_scheme');
      $path = $default_scheme . '://' . preg_replace('/^\//', '', $path);
    }
    return $path;
  }

}
