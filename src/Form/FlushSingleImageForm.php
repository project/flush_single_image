<?php

namespace Drupal\flush_single_image\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\flush_single_image\FlushSingleImageInterface;

/**
 * Flush Single Image Form.
 */
class FlushSingleImageForm extends FormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The filesystem helper.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The drupal messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The single image flusher service.
   *
   * @var \Drupal\flush_single_image\FlushSingleImage
   */
  protected $flushSingleImage;

  /**
   * Constructs a new FlushSingleImageForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   * @param \Drupal\flush_single_image\FlushSingleImageInterface $flush_single_image
   *   Flush single image service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, MessengerInterface $messenger, FlushSingleImageInterface $flush_single_image) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->messenger = $messenger;
    $this->flushSingleImage = $flush_single_image;
  }

  /**
   * Inject services.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container object.
   *
   * @return \Drupal\flush_single_image\Form\FlushSingleImageForm|static
   *   Returns new static object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('messenger'),
      $container->get('flush_single_image')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'flush_single_image_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $default_scheme = $this->config('system.file')->get('default_scheme');
    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File URI'),
      '#description' => $this->t('The image URI to flush image styles for. This can also be a relative path in which case the %default_scheme:// scheme will be used.',
        [
          '%default_scheme' => $default_scheme,
        ]
      ),
    ];

    $form['check'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Check Styles'),
    ];

    $form['check']['description'] = [
      '#markup' => '<p class="description">Click "Check Styles" to check which styles have images cached for the provided image path.</p>',
      '#prefix' => '<div id="flush-single-image-description">',
      '#suffix' => '</div>',
      '#title' => $this->t('Check Styles'),
    ];

    $form['check']['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Check Styles'),
      '#ajax' => [
        'callback' => '::checkStyles',
        'wrapper' => 'flush-single-image-description',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Checking styles...'),
        ],
      ],
      '#attributes' => ['class' => ['form-item']],
    ];

    $actions = [
      $this->flushSingleImage::ACTION_UNLINK => t('Unlink'),
      $this->flushSingleImage::ACTION_REGENERATE => t('Regenerate'),
    ];
    $form['action'] = [
      '#title' => t('Action'),
      '#type' => 'select',
      '#options' => $actions,
      '#empty_option' => t('- Select action -'),
      '#description' => t('Select the action to be performed when flushed.'),
      '#default_value' => $this->flushSingleImage::ACTION_UNLINK,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Flush'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (!$form_state->getValue('path')) {
      $form_state->setError($form['path'], $this->t('@name field is required.', ['@name' => $form['path']['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $action = $form_state->getValue('action');
    if (empty($action)) {
      $action = $this->flushSingleImage::ACTION_UNLINK;
    }
    $paths = $this->flushSingleImage->flush($form_state->getValue('path'), $action);
    foreach ($paths as $path) {
      $this->messenger->addMessage($this->t('Flushed @path', ['@path' => $path]));
    }
    $this->messenger->addMessage($this->t('Flushed all images for @path', ['@path' => $form_state->getValue('path')]));

    $form_state->setRebuild(TRUE);
  }

  /**
   * Ajax callback to check which styles an image has cached.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   */
  public static function checkStyles(array &$form, FormStateInterface $form_state) {
    $paths = \Drupal::service('flush_single_image')->getStylePaths($form_state->getValue('path'));
    if ($paths) {
      $element = [
        '#theme' => 'item_list',
        '#title' => t('Styled Images for @path', ['@path' => $form_state->getValue('path')]),
        '#prefix' => '<div id="flush-single-image-description">',
        '#suffix' => '</div>',
        '#items' => [],
      ];
      foreach ($paths as $path) {
        $element['#items'][] = ['#markup' => $path];
      }
    }
    else {
      $element = [
        '#markup' => '<p class="description">There are no image styles cached for this image.</p>',
        '#prefix' => '<div id="flush-single-image-description">',
        '#suffix' => '</div>',
        '#title' => t('Check Styles'),
      ];
    }

    return $element;
  }

}
