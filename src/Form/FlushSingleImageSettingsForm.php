<?php

namespace Drupal\flush_single_image\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to configure flush_single_image settings.
 */
class FlushSingleImageSettingsForm extends ConfigFormBase {

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The router builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routerBuilder;

  /**
   * The Cache Render.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    RouteBuilderInterface $router_builder,
    CacheBackendInterface $cache_render
  ) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->routerBuilder = $router_builder;
    $this->cacheRender = $cache_render;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('router.builder'),
      $container->get('cache.render')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'flush_single_image_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['flush_single_image.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('flush_single_image.settings');

    $form['flush_single_image'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Enabled media types'),
      '#description' => $this->t('Check the media types that represent an image.'),
      '#tree' => TRUE,
    ];

    $media_types = [];
    $types = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
    foreach ($types as $media_type) {
      $media_types[$media_type->id()] = $media_type->label();
    }
    $form['flush_single_image']['media_image_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Media image types'),
      '#options' => $media_types,
      '#default_value' => $config->get('media_image_types') ?? ['image'],
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    $config = $this->config('flush_single_image.settings');

    $config
      ->set('media_image_types', $form_state->getValue('flush_single_image')['media_image_types'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
