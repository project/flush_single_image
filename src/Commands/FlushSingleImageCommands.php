<?php

namespace Drupal\flush_single_image\Commands;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drush\Commands\DrushCommands;
use Drupal\flush_single_image\FlushSingleImage;

/**
 * Flush Single Image Drush Commands.
 */
class FlushSingleImageCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Flush Single Image Service.
   *
   * @var \Drupal\flush_single_image\FlushSingleImage
   */
  protected $flushSingleImage;

  /**
   * Constructs FlushSingleImageCommands.
   *
   * @param \Drupal\flush_single_image\FlushSingleImage $flush_single_image
   *   Flush single image service.
   */
  public function __construct(FlushSingleImage $flush_single_image) {
    parent::__construct();
    $this->flushSingleImage = $flush_single_image;
  }

  /**
   * Flush Single Image Command.
   *
   * @param string $path
   *   The image URI to flush image styles for.
   * @param array $options
   *   Array of options to set on flush command.
   *
   * @command flush_single_image
   * @bootstrap full
   * @aliases fsi
   * @option check-styles
   *   Check if there are images styles available to cache first.
   *
   * @usage flush_single_image:flush
   */
  public function flush(string $path, array $options = ['check-styles' => FALSE]) {
    if (!file_exists($path)) {
      $this->io()->error($this->t('Provided path: @path is not valid. Please make sure you provide valid path.', ['@path' => $path]));
      return;
    }

    $available_paths = $this->flushSingleImage->getStylePaths($path);
    if (empty($available_paths)) {
      $this->io()->success($this->t('No cached image found for @path.', ['@path' => $path]));
      return;
    }

    $delete = FALSE;
    if ($options['check-styles']) {
      foreach ($available_paths as $available_path) {
        $this->io()->success($this->t('Available path @path.', ['@path' => $available_path]));
      }
      $delete = $this->io()->confirm($this->t('Flush Image Style(s)'));
    }

    // Cancel flush.
    if (!$delete && $options['check-styles']) {
      $this->io()->success($this->t('Flush Image Styles cancelled.'));
      return;
    }

    // Flush all available image styles.
    $paths = $this->flushSingleImage->flush($path);
    foreach ($paths as $flushed_path) {
      $this->io()->success($this->t('Flushed @path', ['@path' => $flushed_path]));
    }
    
    $this->io()->success($this->t('Flushed all image styles for @path', ['@path' => $path]));
  }

}
