<?php

namespace Drupal\flush_single_image\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\flush_single_image\FlushSingleImage as FlushSingleImageService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Flushes existent image derivatives from a single image.
 *
 * The source value is the final destination of the file (usually the return
 * value from Migrate Download process plugin).
 *
 * Available configuration keys:
 * - action:
 *   - 'unlink' - (default) Deletes existent image derivatives.
 *   - 'regenerate' - Regenerates existent image derivatives.
 *
 * Examples:
 *
 * @code
 * process:
 *   path_to_file:
 *     -
 *       plugin: file_copy
 *       source:
 *         - /path/to/file.png
 *         - public://new/path/to/file.png
 *     -
 *       plugin: flush_single_image
 *       action: 'regenerate'
 *
 * @endcode
 *
 * This will download /path/to/file.png to public://new/path/to/file.png
 * (replacing it if it already exists (this is the default behavior of
 * file_copy plugin) and flush all image derivatives previously created for
 * image public://new/path/to/file.png
 *
 * @MigrateProcessPlugin(
 *   id = "flush_single_image"
 * )
 */
class FlushSingleImage extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The flush_single_image service.
   *
   * @var \Drupal\flush_single_image\FlushSingleImage
   */
  protected $flushSingleImageService;

  /**
   * Constructs a flush single image process plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\flush_single_image\FlushSingleImage $flushSingleImageService
   *   Flush single image service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    FlushSingleImageService $flushSingleImageService
  ) {
    if (!empty($configuration['action'] &&
      !in_array($configuration['action'], ['unlink', 'regenerate']))) {
      throw new \InvalidArgumentException("The 'action' parameter must be empty, 'unlink' or 'regenerate'");
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->flushSingleImageService = $flushSingleImageService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('flush_single_image')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $action = $this->flushSingleImageService::ACTION_UNLINK;
    if (!empty($this->configuration['action'])) {
      switch ($this->configuration['action']) {

        case 'regenerate':
          $action = $this->flushSingleImageService::ACTION_REGENERATE;
          break;

        case 'unlink':
          $action = $this->flushSingleImageService::ACTION_UNLINK;
          break;
      }
    }
    $this->flushSingleImageService->flush($value, $action);
    return $value;
  }

}
