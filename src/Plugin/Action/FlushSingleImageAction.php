<?php

namespace Drupal\flush_single_image\Plugin\Action;

use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\flush_single_image\FlushSingleImage;
use Drupal\media\MediaInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Flush single image action.
 *
 * @Action(
 *   id = "flush_single_image_action",
 *   label = @Translation("Flush single image"),
 *   type = "media"
 * )
 */
class FlushSingleImageAction extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  use MessengerTrait;

  /**
   * The flush single image service.
   *
   * @var \Drupal\flush_single_image\FlushSingleImage
   */
  protected $flushSingleImage;

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\flush_single_image\FlushSingleImage $flush_single_image
   *   The Flush single image service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    FlushSingleImage $flush_single_image
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->flushSingleImage = $flush_single_image;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('flush_single_image')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($media = NULL) {
    // Make sure we are dealing with media entity.
    if ($media && $media instanceof MediaInterface) {

      // Make sure we have valid bundle.
      $valid_bundles = [
        'image',
      ];
      if (in_array($media->bundle(), $valid_bundles)) {

        // Make sure we have media image field.
        if ($media->hasField('field_media_image') && !$media->get('field_media_image')->isEmpty()) {
          // Get image uri.
          $image_uri = $media->get('field_media_image')->entity->getFileUri();

          // Get action.
          $action = $this->configuration['fsi_action'];
          if (empty($action)) { 
            $action = $this->flushSingleImage::ACTION_UNLINK;
          }

          // Get paths to flush.
          $paths = $this->flushSingleImage->flush($image_uri, $action);
          foreach ($paths as $path) {
            $this->messenger()->addMessage($this->t('Flushed @path', ['@path' => $path]));
          }
          $this->messenger()->addMessage($this->t('Flushed all images for @path', ['@path' => $image_uri]));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    // Check for flush media image permission.
    if (!$account->hasPermission('flush media image')) {
      return FALSE;
    }
    
    /** @var \Drupal\media\MediaInterface $object */
    $result = $object->access('update', $account, TRUE);
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'fsi_action' => FlushSingleImage::ACTION_UNLINK,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['flush_single_image'] = [
      '#type' => 'details',
      '#title' => $this->t('Flush single image'),
      '#open' => TRUE,
    ];

    $actions = [
      $this->flushSingleImage::ACTION_UNLINK => $this->t('Unlink'),
      $this->flushSingleImage::ACTION_REGENERATE => $this->t('Regenerate'),
    ];
    $form['flush_single_image']['fsi_action'] = [
      '#title' => $this->t('Action'),
      '#type' => 'select',
      '#options' => $actions,
      '#empty_option' => $this->t('- Select action -'),
      '#description' => $this->t('Select the action to be performed when flushed.'),
      '#default_value' => $this->flushSingleImage::ACTION_UNLINK,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['fsi_action'] = $form_state->getValue('fsi_action');
  }

}
