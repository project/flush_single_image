<?php

namespace Drupal\flush_single_image;

/**
 * Interface for Flush Single Image Service.
 */
interface FlushSingleImageInterface {

  /**
   * Flush a single image from all styles that may have a version of it.
   *
   * @param string $path
   *   The filename to flush from all styles. This can be a relative path which
   *   will be given the default stream wrapper scheme, or you can include the
   *   full URI (with stream wrapper).
   * @param int $action
   *   Action type used for when flushing image.
   *
   * @return array
   *   Returns the array of paths flushed.
   */
  public function flush(string $path, int $action): array;

  /**
   * Flush a single image from a specific image style that may have a version of it.
   *
   * @param string $path
   *   The filename to flush from specific style. This can be a relative path which
   *   will be given the default stream wrapper scheme or you can include the
   *   full URI (with stream wrapper).
   * @param string $image_style
   *   The image style id.
   * @param int $action
   *   Action type used for when flushing image.
   *
   * @return bool
   *   Returns TRUE if successful, FALSE otherwise.
   */
  public function flushStyle(string $path, string $image_style, int $action): bool;

  /**
   * The image styles currently cached for given image path.
   *
   * @param string $path
   *   The filename to flush from all styles. This can be a relative path which
   *   will be given the default stream wrapper scheme or you can include the
   *   full URI (with stream wrapper).
   *
   * @return array
   *   Returns the array of style paths.
   */
  public function getStylePaths(string $path): array;

}
