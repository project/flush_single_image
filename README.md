## Flush Single Image

Utility module to flush a single image from any image styles it may have
generated for it.

This module provides a simple form to which you can provide a source image
path (e.g. public://assets/foo/bar/image.jpg) and it will flush any styled
image(s) that have been generated.

### Background

This module was originally developed to solve an issue where daily imports from
a closed third party system would replace image content on occasion, but keep the
filenames the same resulting in out of date images in the image style cache.

### Requirements

Only requires Drupal image, which is a part of Core. No other special
requirements are needed.

#### Optional

Though it's not required, if media is enabled, you will be able to flush
image styles that are associated with media images.

### Install/Configuration

Install module like any other contributed module. It is recommended to install
via composer.

See below for an example:

```bash
composer require drupal/flush_single_image
drush en flush_single_image
```

### Usage

A service class is made available that can be used.

See below for an example:

```php
$path = 'public://assets/foo/bar/image.jpg';
$paths = \Drupal::service('flush_single_image')->flush($path);
foreach ($paths as $flushed_path) {
  \Drupal::messenger->addMessage(t('Flushed @path', ['@path' => $flushed_path]));
}
```

### Drush commands

Drush command is provided to help with flushing image styles and image cache.

```bash
flush_single_image:flush <uri/path> --check-styles
```

First argument expects path/uri of image or image style. You can also pass in
the --check-styles option if you wish to see a list of available paths to
flush.

### Configurable Action

"Flush single image" configurable action is provided. By default, it is setup 
to unlink image styles. This can be configured by navigating to the following 
page: `/admin/config/system/actions/configure/flush_single_image_action`

This action can be applied against media entities of type image.

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
